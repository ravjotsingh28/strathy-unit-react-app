import React from 'react';
import {Navbar,Nav,NavItem} from 'react-bootstrap';

const navBar = (props) => {

    return (
        <Navbar inverse fluid collapseOnSelect style={{marginBottom:0}}>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Canadian English Atlas</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullLeft>
            <NavItem  href="/">About</NavItem>
            <NavItem href="/">Add Audio</NavItem>
            <NavItem href="/">Download Audio</NavItem>
            <NavItem href="/">FAQ</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
}



export default navBar;