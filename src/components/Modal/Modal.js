import React from 'react';
import Modal from 'react-awesome-modal';
import StepsWizard from '../StepsWizard/StepsWizard';

const modal = (props) =>{
    return (
            <Modal 
                    visible={props.visible}
                    width={props.width}
                    height={props.height}
                    effect="fadeInUp"
                    onClickAway={() => props.closeModal()}>
                    <StepsWizard />
                </Modal>
        );
    }

export default modal;