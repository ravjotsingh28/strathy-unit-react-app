/* global google */
import React from 'react';
import {SearchBox} from 'react-google-maps/lib/components/places/SearchBox';


const MapSearchBox = (props) => {

    return (
        <SearchBox  ref={props.onSearchBoxMounted}
                    bounds={props.bounds}
                    controlPosition={google.maps.ControlPosition.TOP_LEFT}
                    onPlacesChanged={props.onPlacesChanged}>
                        <input  type="text"
                                placeholder="Enter Location"
                                style={{
                                boxSizing: `border-box`,
                                border: `1px solid transparent`,
                                width: `240px`,
                                height: `40px`,
                                marginTop: `10px`,
                                padding: `0 12px`,
                                borderRadius: `3px`,
                                boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                                fontSize: `14px`,
                                outline: `none`,
                                textOverflow: `ellipses`,
                                cursor: `text`
                            }}/>
      </SearchBox>
    );

}

export default MapSearchBox;