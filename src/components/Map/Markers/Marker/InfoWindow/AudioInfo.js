import React from 'react';
import {InfoWindow} from 'react-google-maps';

const audioInfo = (props) => {
    return (
        <InfoWindow onCloseClick={ () => {props.onToggleOpen(props.index);}}>
            <div><h1>{props.index}</h1></div>
        </InfoWindow>
    );
}

export default audioInfo;
