import React from 'react';
import {Marker} from 'react-google-maps';
import AudioInfo from './InfoWindow/AudioInfo';

const marker = (props) => {
    let audioInfo = null;
    if(props.marker.isOpen){
        audioInfo=<AudioInfo onToggleOpen = {props.onToggleOpen} index={props.index} />;
    }
    return(<Marker key={props.index} 
            position={props.marker.position} 
            onClick={ () => {props.onToggleOpen(props.index);} } >
            {audioInfo};
    </Marker>);
}

export default marker;
