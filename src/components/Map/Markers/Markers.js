import React from 'react';
import Marker from './Marker/Marker';

const markers = (props) => {
    const markers = props.markers.map((marker, index) => <Marker key={index} marker={marker} onToggleOpen={props.onToggleOpen} index={index}/>);
    return markers;
}

export default markers;