/* global google */
import React,{Component} from 'react';
import {GoogleMap,withScriptjs,withGoogleMap} from 'react-google-maps';
import MapSearchBox from './MapSearchBox/MapSearchBox';
import Markers from './Markers/Markers';
import _ from 'lodash';
import Aux from '../../hoc/Aux';
import Modal from '../Modal/Modal';

class MyMap extends Component{

    state = {
        bounds: null,
        center: {
            lat: 61.6517271, lng: -93.355123
        },
        markers: [],
        zoom:4,
        showModal:false
    };

      onMapMounted = ref => {
        this.setState({map: ref});
      }

      onZoomMounted = ref => {
        this.setState({zoom: ref});
      }

      onBoundsChanged =  () => {
        this.setState({
          bounds: this.state.map.getBounds(),
          center: this.state.map.getCenter(),
        })
      }

      onSearchBoxMounted = ref => {
        this.setState({searchBox:ref});
      }

      onPlacesChanged = () => {
        const places = this.state.searchBox.getPlaces();
        const bounds = new google.maps.LatLngBounds();

        places.forEach(place => {
          if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport)
          } else {
            bounds.extend(place.geometry.location)
          }
        });
        const nextMarkers = places.map(place => ({
          position: place.geometry.location,
          isOpen:false
        }));
        const nextCenter = _.get(nextMarkers, '0.position', this.state.center);
        
        const currentMarkers = [...this.state.markers].concat(nextMarkers);

        let zoomValue = 12;


        this.setState({
          center: nextCenter,
          markers: currentMarkers,
          zoom: zoomValue,
          showModal:true
        });

        

        // refs.map.fitBounds(bounds);
      }

      onZoomChanged = () => {
        this.setState({ zoom: this.state.map.getZoom() });
      }


      onToggleOpen =  (index) => {
        this.setState({isOpen: !this.state.isOpen});

        const updatedMarkers = [...this.state.markers];
        updatedMarkers[index].isOpen = !updatedMarkers[index].isOpen;

        this.setState({markers:updatedMarkers});

      }

      closeModal = () =>{
        this.setState({showModal:false});
      }

    render(){
        return (
            <Aux>
            <GoogleMap  ref={this.onMapMounted}
                        zoom={this.state.zoom}
                        onZoomChanged={this.onZoomChanged}
                        defaultZoom={this.state.zoom}
                        center={this.state.center}
                        onBoundsChanged={this.onBoundsChanged}
                        defaultOptions={{mapTypeControl: false , streetViewControl:false,		
                        mapTypeId : google.maps.MapTypeId.ROADMAP,
                        fullscreenControl: false    }}>
                            <MapSearchBox   onSearchBoxMounted={this.onSearchBoxMounted}
                                            bounds={this.state.bounds}
                                            onPlacesChanged={this.onPlacesChanged}/>

                            <Markers markers={this.state.markers} onToggleOpen={this.onToggleOpen}/>
            </GoogleMap>
            <Modal visible={this.state.showModal}
                    width={'80%'}
                    height={'45%'}
                    closeModal={this.closeModal}/>
            </Aux>

        );
    }
}

export default withScriptjs( withGoogleMap(MyMap));