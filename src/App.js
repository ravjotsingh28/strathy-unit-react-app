import React, { Component } from 'react';
import './App.css';
import NavBar from './components/NavBar/NavBar';
import MyMap from './components/Map/MyMap';

class App extends Component {
  
  state = {
    googleMapAttrs:{
      googleMapURL:"https://maps.googleapis.com/maps/api/js?key=AIzaSyDGypUeicTUNhGdEhWueQ7YqTTkXJzwuCs&v=3.exp&libraries=geometry,drawing,places",
      loadingElement:<div style={{ height: `100%` }} />,
      containerElement:<div style={{ height: `95vh` }} />,
      mapElement:<div style={{ height: `100%` }} />
    }
  }

  render() {
    return (
      <div >
         <NavBar/>
          <MyMap googleMapURL={this.state.googleMapAttrs.googleMapURL}
                loadingElement= {this.state.googleMapAttrs.loadingElement}
                containerElement= {this.state.googleMapAttrs.containerElement}
                mapElement= {this.state.googleMapAttrs.mapElement}/>
      </div>
    );
  }

}

export default App;
